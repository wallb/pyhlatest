

# certi4-hla-build ###############################################
# This stage is for building the CERTI executables.

FROM ubuntu:18.04 AS certi4-hla-build
WORKDIR /usr/hla

# Based on instructions in https://github.com/icyphy/ptII/blob/master/org/hlacerti/build-certi1516e.sh.

# Get required packages
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get install -y \
            bison \
            build-essential \
            ca-certificates \
            cmake \
            flex \
            git \
            libxml2-dev \
    && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

# Get CERTI source
ENV CERTI=/usr/hla/certi
RUN git clone https://git.savannah.nongnu.org/git/certi.git $CERTI

# Build CERTI
WORKDIR $CERTI
RUN cmake $CERTI && \
    make && \
    make install


# certi4-hla-python-build #########################################
# This stage builds the PyHLA files.

FROM certi4-hla-build AS certi4-hla-python-build

# Install required packages
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get install -y python3.6-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

# Build PyHLA
ENV PYHLA=/usr/hla/pyHLA
WORKDIR /usr/hla
RUN git clone https://git.savannah.nongnu.org/git/certi/applications.git /usr/hla/tmp
# The git repository contains various files unrelated to PyHLA, so remove these
RUN mv /usr/hla/tmp/PyHLA $PYHLA && \
    rm -rf /usr/hla/tmp

WORKDIR $PYHLA/build
# If the PYTHON_PACKAGES_PATH is not provided, the cmake script will generate an erroneous one....
RUN cmake -DPYTHON_PACKAGES_PATH=/usr/lib/python3/dist-packages .. && \
    make && \
    make install


# certi4-hla-python ##############################################
# This stage creates a base image for running Python federates.

FROM python:3.6-slim-buster AS certi4-hla-python

# Get required packages
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get install -y libxml2 && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

# Copy the required files from the certi4-hla-python-build stage.
ENV PYHLA=/usr/hla/pyHLA
COPY --from=certi4-hla-python-build /usr/lib/python3/dist-packages/hla /usr/lib/python3/dist-packages/hla

ENV PYTHONPATH=$PYTHONPATH:PYHLA/PyHLA-1.1.1-Source:/usr/lib/python3/dist-packages:/usr/local/lib/python3/dist-packages

# Copy the CERTI executables from the certi4-hla-build stage
ENV CERTI=/usr/hla/certi
WORKDIR $CERTI
COPY --from=certi4-hla-build $CERTI $CERTI
COPY --from=certi4-hla-build /usr/local /usr/local

# Set up environment variables, using a provided script plus some extra. See also: http://www.nongnu.org/certi/certi_doc/Install/html/index.html.
RUN /bin/bash scripts/myCERTI_env.sh
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
ENV CERTI_TCP_PORT 60400
ENV CERTI_FOM_PATH=$CERTI_FOM_PATH:.


# certi4-hla-python-model #######################################
# This stage creates the image used to run the simulation

FROM certi4-hla-python AS certi4-hla-python-model

COPY uav-send.py uav-receive.py uav.fed startup.sh .

# Execute the script file, using the environment variable app, if provided.
CMD ["./startup.sh"]
