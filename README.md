# Test of HLA interactions using pyHLA

## Build

    docker build -t hla-test .

## Run

    docker run hla-test

## Exit

Use e.g. Docker dashboard to stop and delete container
