#!/bin/bash

# This script starts the hla-test


echo "Starting RTIG"
rtig -l 0.0.0.0 &
sleep 3


echo "Starting uav-send"
python -u uav-send.py &
sleep 2


echo "Starting uav-receive"
python -u uav-receive.py
# PYTHONMALLOC=debug python -X faulthandler -u uav-receive.py
