#!/usr/bin/env python
# Requires PyHLA, see http://www.nongnu.org/certi/PyHLA
import hla.rti
import hla.omt as fom

import struct

class MyAmbassador(hla.rti.FederateAmbassador):
    def initialize(self):
        self.classHandle = rtia.getObjectClassHandle("SampleClass")

        self.textAttributeHandle = rtia.getAttributeHandle("TextAttribute", self.classHandle)
        self.structAttributeHandle = rtia.getAttributeHandle("StructAttribute", self.classHandle)
        self.fomAttributeHandle = rtia.getAttributeHandle("FOMAttribute", self.classHandle)
        self.fomValue = 0

        rtia.subscribeObjectClassAttributes(self.classHandle,
            [self.textAttributeHandle, self.structAttributeHandle, self.fomAttributeHandle])

        self.msgInteractionHandle = rtia.getInteractionClassHandle("msg")
        rtia.subscribeInteractionClass(self.msgInteractionHandle)

        print("Receiver - Federate initialized")

    # RTI callbacks
    def discoverObjectInstance(self, object, objectclass, name):
        print("Receiver - DISCOVER", name)
        rtia.requestObjectAttributeValueUpdate(object,
            [self.textAttributeHandle, self.structAttributeHandle, self.fomAttributeHandle])

    def reflectAttributeValues(self, object, attributes, tag, order, transport, time=None, retraction=None):
        #if self.textAttributeHandle in attributes:
        #    print("Receiver - REFLECT", attributes[self.textAttributeHandle])

        #if self.structAttributeHandle in attributes:
        #    structValue = struct.unpack('hhl', attributes[self.structAttributeHandle])
        #    print("Receiver - REFLECT", structValue)

        if self.fomAttributeHandle in attributes:
            fomValue, size = fom.HLAfloat32BE.unpack(attributes[self.fomAttributeHandle])
            print("Receiver - REFLECT", fomValue)
            self.fomValue = fomValue

    def receiveInteraction(self, class_handle, params, tag, orderType, transport_type, time=None, retraction=None):
        print("Receiver - RECEIVED INTERACTION")
        print("Receiver - adress of event retraction handle is", hex(id(retraction)))
        

print("Receiver - Create ambassador")
rtia = hla.rti.RTIAmbassador()

try:
    rtia.createFederationExecution("uav", "uav.fed")
    print("Receiver - Federation created.")
except hla.rti.FederationExecutionAlreadyExists:
    print("Receiver - Federation already exists.")

mya = MyAmbassador()
rtia.joinFederationExecution("uav-receive", "uav", mya)

mya.initialize()

try:
    while(1):
        #foo = 1 + mya.fomValue
        #print("foo is now:", foo)
        rtia.tick(0.5, 0.5)
except KeyboardInterrupt:
    pass

rtia.resignFederationExecution(hla.rti.ResignAction.DeleteObjectsAndReleaseAttributes)

print("Receiver - Done.")
