#!/usr/bin/env python

import time
# Requires PyHLA, see http://www.nongnu.org/certi/PyHLA
import hla.rti
import hla.omt as fom

import struct

class MyAmbassador(hla.rti.FederateAmbassador):
    def initialize(self):
        self.classHandle = rtia.getObjectClassHandle("SampleClass")

        self.textAttributeHandle = rtia.getAttributeHandle("TextAttribute", self.classHandle)
        self.structAttributeHandle = rtia.getAttributeHandle("StructAttribute", self.classHandle)
        self.fomAttributeHandle = rtia.getAttributeHandle("FOMAttribute", self.classHandle)

        rtia.publishObjectClass(self.classHandle,
            [self.textAttributeHandle, self.structAttributeHandle, self.fomAttributeHandle])
        self.myObject = rtia.registerObjectInstance(self.classHandle, "HAF")

        self.msgInteractionHandle = rtia.getInteractionClassHandle("msg")
        rtia.publishInteractionClass(self.msgInteractionHandle)

        print("Sender - Federate initialized")

    def terminate(self):
        rtia.deleteObjectInstance(self.myObject, "HAF")

    # RTI callbacks
    def startRegistrationForObjectClass(*params):
        print("Sender - START", params)

    def provideAttributeValueUpdate(*params):
        print("Sender - PROVIDE UAV", params)

print("Sender - Create ambassador")
rtia = hla.rti.RTIAmbassador()
print(rtia)

try:
    rtia.createFederationExecution("uav", "uav.fed")
    print("Sender - Federation created.")
except hla.rti.FederationExecutionAlreadyExists:
    print("Sender - Federation already exists.")

mya = MyAmbassador()
rtia.joinFederationExecution("uav-send", "uav", mya)

mya.initialize()

try:
    a= 3.14
    while(1):
        a = a + 1.0
        print("Sender - Updating attribute")
        rtia.updateAttributeValues(mya.myObject,
            {mya.textAttributeHandle:"text",
            mya.structAttributeHandle:struct.pack('hhl', 1, 2, 3),
            mya.fomAttributeHandle:fom.HLAfloat32BE.pack(a)},
            "update")
        if int(time.time()) % 2 == 0:
            print("Sender - Sending msg")
            rtia.sendInteraction(mya.msgInteractionHandle, dict(), "msg")
        rtia.tick(0.5, 0.5)
except KeyboardInterrupt:
    pass

mya.terminate()

rtia.resignFederationExecution(hla.rti.ResignAction.DeleteObjectsAndReleaseAttributes)

print("Sender - Done.")
